﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Grid
{
	public partial class GridForm : Form
	{
		#region disable exit button on this form
		//this code disables the exit button on top right corner for this form
		private const int CP_NoClose_Button = 0x200;
		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams myCp = base.CreateParams;
				myCp.ClassStyle = myCp.ClassStyle | CP_NoClose_Button;
				return myCp;
			}
		}
		#endregion
		private int i = 0, l = 0;
		private Menu menu;
		private GridManager gridManager;
		private Graphics dc;
		private Wizard wizard;
		private AStar aStar;
		private List<Node> path;
		private DepthFirstSearch dfs;
		private List<Node> keyNodes = new List<Node>();

		public GridForm(Menu menu)
		{
			InitializeComponent();
			ClientSize = new Size(800, 840);
			Rectangle renderSurface = new Rectangle(0, 40, 800, 800);
			KeyDown += new KeyEventHandler(Form1_KeyDown);
			this.menu = menu;


			dc = CreateGraphics();
			gridManager = new GridManager(dc, renderSurface);
			Loop.Enabled = true;
			wizard = new Wizard(gridManager.Grid[18].Position, gridManager.NodeSize, gridManager);
			aStar = new AStar();


			//runs DFS
			int tries = 0;
			dfs = new DepthFirstSearch(gridManager.Grid);
			while (keyNodes.Count < 2 && tries < 10)
			{
				try
				{
					dfs.DFS();
					keyNodes = dfs.KeyNodes;
				}
				catch { }
				finally
				{
					tries++;
				}
			}
		}

		private void Loop_Tick(object sender, EventArgs e)
		{
			//Draws all our nodes
			gridManager.Render();
			wizard.Render(dc);
		}

		private void Form1_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				StartButton_Click(sender, e);
			}

		}

		private void StepTimer_Tick(object sender, EventArgs e)
		{
			if (path != null)
			{
				if (i != path.Count && i < path.Count)
				{
					wizard.Position = path[i].Position;
					i++;
				}
				else
				{
					i = 0;
					path = null;
				}

				if (wizard.Position == keyNodes[0].Position)
				{
					keyNodes[0].InfoSprite = null;
				}
				if (wizard.Position == keyNodes[1].Position)
				{
					keyNodes[1].InfoSprite = null;
				}
			}
			else
			{
				GeneratePath();
			}
		}

		private void GeneratePath()
		{
			switch (l)
			{
				case 0:
					path = aStar.FindPath(gridManager.Grid[18], keyNodes[0]);

					break;

				case 1:
					foreach (Node node in gridManager.Grid)
					{
						node.Parent = null;
					}
					path = aStar.FindPath(keyNodes[0], keyNodes[1]);
					break;

				case 2:
					foreach (Node node in gridManager.Grid)
					{
						node.Parent = null;
					}
					path = aStar.FindPath(keyNodes[1], gridManager.Grid[24]);
					break;

				case 3:
					foreach (Node node in gridManager.Grid)
					{
						node.Parent = null;
					}
					path = aStar.FindPath(gridManager.Grid[25], gridManager.Grid[88]);
					break;

				case 4:
					foreach (Node node in gridManager.Grid)
					{
						node.Parent = null;
					}
					path = aStar.FindPath(gridManager.Grid[88], gridManager.Grid[18]);
					break;

				default:
					break;
			}
			l++;
		}
		#region GUI
		private void BackButton_Click(object sender, EventArgs e)
		{
			menu.Show();
			this.Close();
		}

		private void StartButton_Click(object sender, EventArgs e)
		{
			if (StepTimer.Enabled)
			{
				StepTimer.Enabled = false;
				StartButton.Text = "Start";
			}
			else
			{
				StepTimer.Enabled = true;
				StartButton.Text = "Pause";
			}
		}

		private void ExitButton_Click(object sender, EventArgs e)
		{
			menu.Exit();
		}

		private void ResetButton_Click(object sender, EventArgs e)
		{
			menu.StartButton_Click(sender, e);
			this.Close();
		}
		#endregion
	}
}
