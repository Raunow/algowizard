﻿using System.Collections.Generic;
using System.Drawing;

namespace Grid
{
	class Node
	{
		#region Fields
		private bool isWalkable = true, hasKey = false, isForestPath = false, isCheckedDFS = false, isStructure = false;
		private Point position;
		private int nodeSize;
		private Image tileSprite, infoSprite;
		private List<Node> surNodes = new List<Node>(); //surrounding nodes
		private Node parent, parentDFS;
		//A*
		float h = 0, g = 0, f = 0;
		#endregion

		#region Properties
		public Rectangle BoundingRectangle
		{
			get
			{
				return new Rectangle(position.X * nodeSize, (position.Y * nodeSize) + 40, nodeSize, nodeSize);
			}
		}
		public Point Position { get => position; set => position = value; }
		public bool IsWalkable { get => isWalkable; set => isWalkable = value; }
		public bool HasKey { get => hasKey; set => hasKey = value; }
		public bool IsForestPath { get => isForestPath; set => isForestPath = value; }
		public Image InfoSprite { set => infoSprite = value; }

		//A*
		public float G { get => g; set => g = value; }
		public float H { get => h; set => h = value; }
		public float F { get => f; set => f = value; }

		public List<Node> SurNodes { get => surNodes;}
		public Node Parent { get => this.parent; set => parent = value; }

		//DFS
		internal Node ParentDFS { get => parentDFS; set => parentDFS = value; }
		public bool IsCheckedDFS { get => isCheckedDFS; set => isCheckedDFS = value; }
		public bool IsStructure { get => isStructure; set => isStructure = value; }
		#endregion


		public Node(Point position, int size)
		{
			this.position = position;
			this.nodeSize = size;

			//selects the sprite depending on location
			SelectSprite();
		}


		#region Sprite
		private void SelectSprite()
		{
			if (position.X == 2 && position.Y == 4)
			{
				tileSprite = Image.FromFile(@"Images\Tower.png");
				isWalkable = true;
				isStructure = true;
			}
			else if (position.X == 8 && position.Y == 7)
			{
				tileSprite = Image.FromFile(@"Images\SecondTower.png");
				isWalkable = true;
				isStructure = true;
			}
			else if (position.X == 0 && position.Y == 8)
			{
				tileSprite = Image.FromFile(@"Images\Portal.png");
				isWalkable = false;
				isStructure = true;
			}
			else if (position.X > 3 && position.X < 7 && position.Y > 0 && position.Y < 7)
			{
				tileSprite = Image.FromFile(@"Images\Wall.png");
				isWalkable = false;
			}
			else if (position.Y == 7 && position.X > 1 && position.X < 8 || position.Y == 9 && position.X > 1 && position.X < 8)
			{
				tileSprite = Image.FromFile(@"Images\tree.png");
				isWalkable = false;
			}
			#region else if (...) //Extend region
			else if (((position.X == 3 || position.X == 7) && position.Y >= 0 && position.Y < 5) ||
					((position.X == 1 || position.X == 8) && position.Y > 4 && position.Y < 9) ||
					(position.X > 1 && position.X < 8 && (position.Y == 5 || position.Y == 8)) ||
					(position.X > 3 && position.X < 7 && position.Y == 0))
			#endregion
			{
				tileSprite = Image.FromFile(@"Images\PathTile.png");

			}
			//if its the forest path
			if (position.Y == 8 && position.X > 1 && position.X < 8)
			{
				isForestPath = true;
			}
		}

		public void Render(Graphics dc)
		{
			//Draws the rectangles border
			dc.DrawRectangle(new Pen(Color.Black), BoundingRectangle);

			//If the node has a sprite, then we need to draw it
			if (tileSprite != null)
			{
				dc.DrawImage(tileSprite, BoundingRectangle);
			}

			//if the node got a key draw it
			if (infoSprite != null)
			{
				dc.DrawImage(infoSprite, BoundingRectangle);
			}

			#region ForestPathRelated
			//if its forrestpath, and blocked tile is true
			//forrest have been walked and monsters will apear
			if (isForestPath && !isWalkable)
			{
				switch (position.X)
				{
					case 3:
						//sets the sprites and draws it
						infoSprite = Image.FromFile(@"Images\PurpleMonster.png");
						dc.DrawImage(infoSprite, BoundingRectangle);
						break;
					case 5:
						//sets the sprites and draws it
						infoSprite = Image.FromFile(@"Images\RedMonster.png");
						dc.DrawImage(infoSprite, BoundingRectangle);
						break;
					case 6:
						//sets the sprites and draws it
						infoSprite = Image.FromFile(@"Images\threeEyeMonster.png");
						dc.DrawImage(infoSprite, BoundingRectangle);
						break;
				}
			}
			#endregion

			//Write's the nodes grid position
			dc.DrawString("" + position.X + ", " + position.Y, new Font("Arial", 7, FontStyle.Regular), new SolidBrush(Color.Black), (position.X * nodeSize) + 2, (position.Y * nodeSize) + 50);

		}
		#endregion

		#region Grid
		public void AddSurNode(Node node)
		{
			SurNodes.Add(node);
		}
		#endregion
	}
}
