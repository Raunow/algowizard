﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Grid
{
	class GridManager
	{
		//Manageing graphics
		private BufferedGraphics backBuffer;
		private Graphics dc;
		private Rectangle displayRectangle;
		private Image bg;

		private int nodeRowCount;
		private int nodeSize;
		private Random rnd = new Random();

		private List<Node> grid;

		internal List<Node> Grid { get => grid; set => grid = value; }
		public int NodeSize { get => nodeSize; set => nodeSize = value; }

		
		public GridManager(Graphics dc, Rectangle displayRectangle)
		{
			//Create's (Allocates) a buffer in memory with the size of the display
			this.backBuffer = BufferedGraphicsManager.Current.Allocate(dc, displayRectangle);

			//Sets the graphics context to the graphics in the buffer
			this.dc = backBuffer.Graphics;

			//Sets the displayRectangle
			this.displayRectangle = displayRectangle;

			bg = Image.FromFile(@"Images\BackGround.png");


			//Sets the row count to then, this will create a 10 by 10 grid.
			nodeRowCount = 10;

			CreateGrid();
			LinkGrid();
			Addkeys();
		}

		/// <summary> Renders all the nodes </summary>
		public void Render()
		{
			dc.DrawImage(bg, displayRectangle);

			foreach (Node node in grid)
			{
				node.Render(dc);
			}

			//Renders the content of the buffered graphics context to the real context(Swap buffers)
			backBuffer.Render();
		}

		/// <summary> Creates the grid </summary>
		public void CreateGrid()
		{
			//Instantiates the list of nodes
			grid = new List<Node>();

			//Sets the node size
			nodeSize = displayRectangle.Width / nodeRowCount;

			//Creates all the nodes
			for (int x = 0; x < nodeRowCount; x++)
			{
				for (int y = 0; y < nodeRowCount; y++)
				{
					grid.Add(new Node(new Point(x, y), nodeSize));
				}
			}
		}

		private void LinkGrid()
		{
			for (int i = 0; i < grid.Count; i++)
			{
				for (int j = 0; j < grid.Count; j++)
				{
					if ((grid[j].Position.X <= grid[i].Position.X + 1 && grid[j].Position.X >= grid[i].Position.X - 1) &&
						(grid[j].Position.Y <= grid[i].Position.Y + 1 && grid[j].Position.Y >= grid[i].Position.Y - 1) &&
						grid[j] != grid[i] && grid[j].IsWalkable)
					{
						grid[i].AddSurNode(grid[j]);
					}
				}
			}
		}

		///<summary> Spawns keys on available/walkable tiles </summary>
		public void Addkeys()
		{
			int placed = 0;

			//runs until 2 spots is found for keys
			while (placed < 2)
			{
				Point keyPos = new Point(rnd.Next(0, 10), rnd.Next(0, 10));

				//Loops through all 100 Nodes
				foreach (Node node in grid)
				{
					//checks if the coords is the same as the random gen coords
					if (node.Position == keyPos)
					{
						//check if that cord is walkable
						if (node.HasKey = node.IsWalkable && !node.HasKey && !node.IsStructure)
						{
							node.InfoSprite = Image.FromFile(@"Images\Key.png");
							//makes coord add key
							//count up placed keys
							placed++;
							//key was placed and foreach breaked
							break;
						}
						else
						{
							//because the random cord was found and wasnt walkable
							//it breaks the foreach and start a new random cord
							break;
						}
					}
				}
			}
		}
	}
}

