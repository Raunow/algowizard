﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grid
{
	class AStar
	{
		Node endNode, startNode;
		List<Node> open;
		List<Node> closed;


		public List<Node> FindPath(Node startNode, Node endNode)
		{
			this.startNode = startNode;
			this.endNode = endNode;

			List<Node> path = new List<Node>();
			open = new List<Node>();
			closed = new List<Node>();

			bool succes = Search(startNode);
			if (succes)
			{
				path = closed;
			}

			return path;
		}

		//anything below this line hasn't been tested--------------------------
		private bool Search(Node currentNode)
		{
			if (!closed.Contains(currentNode))
			{
				closed.Add(currentNode);
			}

			foreach (Node node in currentNode.SurNodes)
			{
				if (!open.Contains(node))
				{
					open.Add(node);
					node.G = TravelCost(currentNode.Position, node.Position);
					node.H = TravelCost(node.Position, endNode.Position);
					node.F = node.G + node.H;
					node.Parent = currentNode;
				}
			}
			
			if (currentNode.Position == this.endNode.Position)
			{
				return true;
			}
			else
			{
				if (Search(FindBestPath(open)))
				{
					return true;
				}
				
			}

			return false;
		}

		public int TravelCost(Point start, Point goal)
		{
			int x, y;
			x = goal.X - start.X;
			x *= x;
			y = goal.Y - start.Y;
			y *= y;

			return (int)Math.Sqrt(x + y);
		}

		private Node FindBestPath(List<Node> nodes)
		{
			Node temp = nodes[0];

			foreach (Node node in nodes)
			{
				if (node.F < temp.F)
				{
					temp = node;
				}
				else if (node.F == temp.F)
				{
					if (node.H < temp.H)
					{
						temp = node;
					}
				}
			}

			return temp;
		}
	}
}
