﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grid
{
	class Wizard
	{
		#region MyRegion
		private Image sprite;
		private Point position;
		private int nodeSize;
		private Node lastNode, currentNode;
		private GridManager gridManager;
		#endregion

		#region Properties
		public Rectangle BoundingRectangle
		{
			get
			{
				return new Rectangle(position.X * nodeSize, position.Y * nodeSize + 35, nodeSize, nodeSize);
			}
		}
		public Point Position { get => position; set => position = value; }
		#endregion

		#region Constructor
		public Wizard(Point pos, int size, GridManager gManager)
		{
			gridManager = gManager;
			position = pos;
			nodeSize = size;
			sprite = Image.FromFile(@"Images\Wizard.png");
		}
		#endregion

		#region Methods
		public void Render(Graphics dc)
		{
			dc.DrawImage(sprite, BoundingRectangle);
		}

		public void IsForrest(Node cNode)
		{
			lastNode = currentNode;
			currentNode = cNode;
			//checks if last path was forrest and current isnt.. if true block forrest
			if (lastNode.IsForestPath && !currentNode.IsForestPath)
			{
				foreach (var cell in gridManager.Grid)
				{
					if(cell.IsForestPath)
					{
						cell.IsWalkable = false;
					}
				}
			}
		}
		#endregion
	}
}
