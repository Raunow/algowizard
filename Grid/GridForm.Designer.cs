﻿namespace Grid
{
	partial class GridForm
	{
		private System.ComponentModel.IContainer components = null;
		
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.Loop = new System.Windows.Forms.Timer(this.components);
			this.StepTimer = new System.Windows.Forms.Timer(this.components);
			this.BackButton = new System.Windows.Forms.Button();
			this.StartButton = new System.Windows.Forms.Button();
			this.ExitButton = new System.Windows.Forms.Button();
			this.ResetButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// Loop
			// 
			this.Loop.Tick += new System.EventHandler(this.Loop_Tick);
			// 
			// StepTimer
			// 
			this.StepTimer.Interval = 500;
			this.StepTimer.Tick += new System.EventHandler(this.StepTimer_Tick);
			// 
			// BackButton
			// 
			this.BackButton.Location = new System.Drawing.Point(616, 12);
			this.BackButton.Name = "BackButton";
			this.BackButton.Size = new System.Drawing.Size(75, 23);
			this.BackButton.TabIndex = 3;
			this.BackButton.Text = "Back";
			this.BackButton.UseVisualStyleBackColor = true;
			this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
			// 
			// StartButton
			// 
			this.StartButton.Location = new System.Drawing.Point(13, 12);
			this.StartButton.Name = "StartButton";
			this.StartButton.Size = new System.Drawing.Size(75, 23);
			this.StartButton.TabIndex = 1;
			this.StartButton.Text = "Start";
			this.StartButton.UseVisualStyleBackColor = true;
			this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
			// 
			// ExitButton
			// 
			this.ExitButton.Location = new System.Drawing.Point(697, 13);
			this.ExitButton.Name = "ExitButton";
			this.ExitButton.Size = new System.Drawing.Size(75, 23);
			this.ExitButton.TabIndex = 4;
			this.ExitButton.Text = "Exit";
			this.ExitButton.UseVisualStyleBackColor = true;
			this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
			// 
			// ResetButton
			// 
			this.ResetButton.Location = new System.Drawing.Point(95, 12);
			this.ResetButton.Name = "ResetButton";
			this.ResetButton.Size = new System.Drawing.Size(75, 23);
			this.ResetButton.TabIndex = 2;
			this.ResetButton.Text = "Reset";
			this.ResetButton.UseVisualStyleBackColor = true;
			this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
			// 
			// GridForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 821);
			this.Controls.Add(this.ResetButton);
			this.Controls.Add(this.ExitButton);
			this.Controls.Add(this.StartButton);
			this.Controls.Add(this.BackButton);
			this.Name = "GridForm";
			this.Text = "Algorithm";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Timer Loop;
		private System.Windows.Forms.Timer StepTimer;
		private System.Windows.Forms.Button BackButton;
		private System.Windows.Forms.Button StartButton;
		private System.Windows.Forms.Button ExitButton;
		private System.Windows.Forms.Button ResetButton;
	}
}

