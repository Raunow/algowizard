﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grid
{
	class DepthFirstSearch
	{
		#region Fields
		private List<Node> grid;
		//DPS
		private Node currentNode, nodeInSpotlight;
		private bool searchDone;
		//Result
		private List<Node> keyNodes = new List<Node>();
		#endregion

		#region Properties
		internal List<Node> KeyNodes { get => keyNodes; set => keyNodes = value; }
		#endregion

		#region Contructor
		public DepthFirstSearch(List<Node> grid)
		{
			this.grid = grid;
			//starts at the first node in the grid list
			currentNode = grid[0];
		}
		#endregion

		#region Methods
		public void DFS()
		{
			//checks until all nodes are visited
			while (!searchDone)
			{
				//checks if the node got a key
				if (currentNode.HasKey)
				{
					keyNodes.Add(currentNode);
				}
				//sets the current node's isChecked = true
				currentNode.IsCheckedDFS = true;

				#region How the node checks the surrounding nodes
				// 0 | 1 | 2
				//-----------
				// 3 |   | 4
				//-----------
				// 5 | 6 | 7
				#endregion

				int i = 0;
				foreach (var node in currentNode.SurNodes)
				{
					i++;
					//if node in spotlight isnt checked:
					if (!node.IsCheckedDFS)
					{
						nodeInSpotlight = node;
						//got the info it needs, and jumps out the loop
						break;
					}
					//if loop gets to 7 and its either null or checked DPS is done
					else if (i == currentNode.SurNodes.Count)
					{
						searchDone = true;
					}
				}
				//nodeinSpotlight's parent = currentnode
				nodeInSpotlight.ParentDFS= currentNode;
				//nodeInSpotlight is made current node and it now runs for that node
				currentNode = nodeInSpotlight;
			}
			try
			{
				if (TravelCost(keyNodes[0].Position, grid[18].Position) > TravelCost(keyNodes[1].Position, grid[18].Position))
				{
					Node temp = keyNodes[0];
					keyNodes[0] = keyNodes[1];
					keyNodes[1] = temp;
				}
			}
			catch { }
		}
		public int TravelCost(Point start, Point goal)
		{
			int x, y;
			x = goal.X - start.X;
			x *= x;
			y = goal.Y - start.Y;
			y *= y;

			return (int)Math.Sqrt(x + y);
		}
		#endregion
	}
}
